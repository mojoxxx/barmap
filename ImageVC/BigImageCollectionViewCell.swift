//
//  BigImageCollectionViewCell.swift
//  BarMap
//
//  Created by Mojo on 2019/7/6.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit

class BigImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
