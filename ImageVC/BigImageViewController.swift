//
//  BigImageViewController.swift
//  BarMap
//
//  Created by Mojo on 2019/7/6.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit
import ImagePicker
import Lightbox
import MapKit
import Photos
import SwiftAssetsPickerController

protocol BigImageViewControllerDelegate {
    func didFinishEditImage(isEdited: Bool)
}

class BigImageViewController: UIViewController {
    
    let serverConnect = ServerConnect.shared
    let editImage = EditImage.shared
    let queue = OperationQueue()
    
    var photos : [UIImage] = []
    var storeImages : [StoreImage] = []
    var storeID : String!
    var delegate : BigImageViewControllerDelegate?
    var urlArray : [URL] = []
    var urlDictionary : [URL:UIImage] = [:]
    var isEdited = false

    @IBOutlet weak var bigImageVC: UICollectionView!
    @IBOutlet weak var bigImageLayout: UICollectionViewFlowLayout!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bigImageVC.delegate = self
        self.bigImageVC.dataSource = self
        bigImageLayout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        guard let width = self.navigationController?.navigationBar.frame.size.width else{
            return
        }
        bigImageLayout.itemSize = CGSize(width: width/2-5, height: width/2-5)
        bigImageLayout.minimumInteritemSpacing = 1 //上下
//        bigImageLayout.minimumLineSpacing = 2 //左右
        bigImageLayout.scrollDirection = .vertical
//        bigImageLayout.headerReferenceSize = CGSize( width: self.bigImageVC.frame.size.width, height: 40)

        
        let item = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(self.back))
        item.image = UIImage(named: "Back")
        self.navigationItem.leftBarButtonItem = item
        
        loadPhoto()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if storeImages.isEmpty{
            self.bigImageVC.backgroundColor = UIColor(patternImage: UIImage(named: "ImageBackgroundImage")!)
//        }
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.delegate?.didFinishEditImage(isEdited: self.isEdited)
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadPhoto(){
        for storeImage in self.storeImages{
            let imageName = storeImage.imageName
            guard let url = URL(string: "\(serverConnect.domain)storeImage/\(imageName)") else{
                assertionFailure("Fail to get url.")
                return
            }
            self.urlArray.append(url)
            DispatchQueue.global().async {
                if let imageData = try? Data(contentsOf: url){
                    DispatchQueue.main.async {
                        guard let image = UIImage(data: imageData) else{
                            assertionFailure("Fail to get image.")
                            return
                        }
                        self.urlDictionary.updateValue(image, forKey: url)
                        if self.urlDictionary.count == self.urlArray.count{
                            for url in self.urlArray{
                                guard let image = self.urlDictionary[url] else{
                                    assertionFailure("Fail to get urlDictionary[url].")
                                    return
                                }
                                self.photos.append(image)
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func photo(_ sender: Any) {
        let rootListAssets = AssetsPickerController()
        rootListAssets.didSelectAssets = {(assets: Array<PHAsset?>) -> () in
            guard let assetsArray = assets as? [PHAsset] else{
                return
            }
            let images = self.getAssetThumbnail(assets: assetsArray)
            self.insertImage(images: images)
        }
        let navigationController = UINavigationController(rootViewController: rootListAssets)
        present(navigationController, animated: true, completion: nil)
    }
    
    //MARK: Convert array of PHAsset to UIImages
    func getAssetThumbnail(assets: [PHAsset]) -> [UIImage] {
        var arrayOfImages = [UIImage]()
        for asset in assets {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            var image = UIImage()
            option.isSynchronous = true
            manager.requestImage(for: asset, targetSize: CGSize(width: assets.first!.pixelWidth, height: assets.first!.pixelHeight), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
                image = result!
                arrayOfImages.append(image)
            })
        }
        
        return arrayOfImages
    }
    
    //MARK: uploadStoreImage & insertStoreImage
    func insertImage(images: [UIImage]){
        
        for image in images{
            let fileName = String(format: "%@.jpg", UUID().uuidString)
            let storeImage = StoreImage(imageName: fileName, storeID: self.storeID)
            
            
            //            guard let thumbnailImage = self.editImage.thumbnailImage(image: image,width :1024,height :1024,circle :false),
            //                let data = image.jpegData(compressionQuality: 0.7),
            guard let data = image.jpegData(compressionQuality: 0.7),
                let encodedData = try? JSONEncoder().encode(storeImage) else{
                    assertionFailure("Convert image or encode storeImage fail.")
                    return
            }
            self.serverConnect.uploadStioreImage(uploadUrl: "\(serverConnect.domain)uploadStoreImage.php?fileName=\(fileName)", imgData: data) { (data) in
                guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else{
                    return
                }
                print("uploadStoreImage \(json["status"]!)")
                self.photos.insert(image, at: 0)
                self.storeImages.insert(storeImage, at: 0)
                DispatchQueue.main.async {
                    self.bigImageVC.reloadData()
                }
            }
            self.serverConnect.sentJsonToServer(urlString: "\(serverConnect.domain)insertStoreImage.php", encodedData: encodedData){ (data) in
                guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                    else{
                        return
                }
                print("insertStoreImage \(String(describing: json["status"]))")
            }
        }
        self.isEdited = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//MARK:UICollectionViewDelegate
extension BigImageViewController:UICollectionViewDelegate{
    
}

//MARK:UICollectionViewDataSource
extension BigImageViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        print("storeImages.count=\(storeImages.count)")
        return storeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BigImageCell", for: indexPath) as! BigImageCollectionViewCell
        
        cell.imageView.image = nil
        let imageName = self.storeImages[indexPath.row].imageName
        guard let url = URL(string: "\(serverConnect.domain)storeImage/\(imageName)") else{
            assertionFailure("Fail to get url.")
            return cell
        }
        
        //非同步 可取消
        //        print("indexPath=\(indexPath)")
        let operation = BigImageOperation(url: url, collectionView: collectionView, indexPath: indexPath)
        queue.addOperation(operation)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //相同位置的operation cancel掉 因為已離開畫面
        self.queue.operations.filter { (op) -> Bool in
            let bigImageOperation = op as! BigImageOperation
            //如果位置一樣，則取消
            if bigImageOperation.indexPath.compare(indexPath) == .orderedSame {
                return true
            }
            return false
            }.first?.cancel()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("row: \(indexPath.row)")
        guard self.photos.count > 0 else { return }
        
        let lightboxImages = self.photos.map {
            return LightboxImage(image: $0)
        }
        
        let lightbox = LightboxController(images: lightboxImages, startIndex: indexPath.row)
//        lightbox.dynamicBackground = true
        lightbox.imageTouchDelegate = self
        lightbox.pageDelegate = self
        lightbox.modalPresentationStyle = .fullScreen
        self.present(lightbox, animated: true, completion: nil)
    }
    
    
}

extension BigImageViewController:ImagePickerDelegate,LightboxControllerTouchDelegate,LightboxControllerPageDelegate{
    func lightboxController(_ controller: LightboxController, didMoveToPage page: Int) {
        
    }
    
    func lightboxController(_ controller: LightboxController, didTouch image: LightboxImage, at index: Int) {
       
    }
    
    func wrapperDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        guard images.count > 0 else { return }
        
        let lightboxImages = images.map {
            return LightboxImage(image: $0)
        }
        
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.modalPresentationStyle = .fullScreen
        imagePicker.present(lightbox, animated: true, completion: nil)
    }
    
    func doneButtonDidPress(_ imagePicker: ImagePickerController, images: [UIImage]) {
        
        self.insertImage(images: images)
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func cancelButtonDidPress(_ imagePicker: ImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func camera(_ sender: Any) {
        let config = Configuration()
        config.doneButtonTitle = "Done"
        config.recordLocation = false
        config.allowVideoSelection = true

        let imagePickerController = ImagePickerController(configuration: config)
        imagePickerController.delegate = self
        imagePickerController.modalPresentationStyle = .fullScreen
        present(imagePickerController, animated: true, completion: nil)
    }
}
