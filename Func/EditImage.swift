//
//  ThumbnailImage.swift
//  BarMap
//
//  Created by Mojo on 2019/6/25.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation
import UIKit

class EditImage {
    static let shared = EditImage()
    
    func thumbnailImage(image: UIImage,width :CGFloat,height :CGFloat,circle :Bool,background: Bool) -> UIImage? {
        let thumbnailSize = CGSize(width:width, height: height); //設定縮圖大小
        let scale = UIScreen.main.scale //找出目前螢幕的scale，視網膜技術為2.0
        
        //產生畫布，第一個參數指定大小,第二個參數true:不透明（黑色底）,false表示透明背景,scale為螢幕scale
        UIGraphicsBeginImageContextWithOptions(thumbnailSize,background,scale)
        
        //計算長寬要縮圖比例，取最大值MAX會變成UIViewContentModeScaleAspectFill
        //最小值MIN會變成UIViewContentModeScaleAspectFit
        let widthRatio = thumbnailSize.width / image.size.width;
        let heightRadio = thumbnailSize.height / image.size.height;
        
        let ratio = max(widthRatio,heightRadio);
        
        let imageSize = CGSize(width:image.size.width*ratio,height: image.size.height*ratio);
        
        //如果要切圓形請加下面兩行
        if circle{
            let circlePath = UIBezierPath(ovalIn: CGRect(x: 0,y: 0,width: thumbnailSize.width,height: thumbnailSize.height))
            circlePath.addClip()
        }
        
        
        
        image.draw(in:CGRect(x: -(imageSize.width-thumbnailSize.width)/2.0,y: -(imageSize.height-thumbnailSize.height)/2.0,
                             width: imageSize.width,height: imageSize.height))
        //取得畫布上的縮圖
        let smallImage = UIGraphicsGetImageFromCurrentImageContext();
        //關掉畫布
        UIGraphicsEndImageContext();
        return smallImage
    }
}
