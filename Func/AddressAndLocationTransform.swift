//
//  AddressAndLocationTransform.swift
//  BarMap
//
//  Created by Mojo on 2019/7/6.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation
import CoreLocation
class AddressAndLocationTransform{
    
    static let shared = AddressAndLocationTransform()
    
    func addressToLocation(address: String,completionHandler : @escaping (CLLocationCoordinate2D) -> Void ){
        //Addrrss -->Lat,Lon
        let gecoder1 = CLGeocoder()
        gecoder1.geocodeAddressString(address) { (placemarks, error) in
            if let error = error {
                print("geocodeAddressString error \(error)")
                return
            }
            guard let placemark = placemarks?.first,
                let coordinate = placemark.location?.coordinate else {
                    assertionFailure("Invalid placemark.")
                    return
            }
            completionHandler(coordinate)
            print("經緯度: \(coordinate.latitude),\(coordinate.longitude)")
        }
    }
    
    
    func locationAddress(latitude: CLLocationDegrees, longitude: CLLocationDegrees, completionHandler : @escaping (String) -> Void ){
        //Lat,Lon -->Addrrss
        let gecoder2 = CLGeocoder()
        let homeLocation = CLLocation(latitude: latitude, longitude: longitude)
        gecoder2.reverseGeocodeLocation(homeLocation) { (placemarks, error) in
            if let error = error {
                print("geocodeAddressString error \(error)")
                return
            }
            guard let placemark = placemarks?.first else {
                assertionFailure("Invalid placemark.")
                return
            }
            let address = "\(placemark.subAdministrativeArea ?? "")\(placemark.locality ?? "")\(placemark.name ?? "")"
            completionHandler(address)
            print("地址:\(placemark.subAdministrativeArea ?? "")\(placemark.locality ?? "")\(placemark.name ?? "")")
        }

    }
}
