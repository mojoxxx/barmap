
import Foundation
import UIKit

class BigImageOperation : Operation{
    var url : URL!
    var collectionView: UICollectionView!
    var indexPath : IndexPath!
    let editImage = EditImage.shared
    
    init(url : URL, collectionView: UICollectionView, indexPath : IndexPath) {
        self.url = url
        self.collectionView = collectionView
        self.indexPath = indexPath
        super.init()
    }
    
    //main方式,預設情況下是執行在背景執行緒
    override func main() {
        
        if self.isCancelled{
            return
        }
        
        
        if let imageData = try? Data(contentsOf: url){
            DispatchQueue.main.async {
                //下載完時，判斷該位置的cell是否在畫面上
                if let cell = self.collectionView.cellForItem(at: self.indexPath)as? BigImageCollectionViewCell {
                    //如果該位置在停留的畫面上，則cell會有值，否則為空值
                    guard let image = UIImage(data: imageData) else{
                        assertionFailure("Fail to get image.")
                        return
                    }
                    cell.imageView.image = self.editImage.thumbnailImage(image: image,width :1024,height :1024,circle :false,background: true)
                    cell.setNeedsLayout()//通知Layout重新排列,coustomCell不會有作用要另外寫
                    
                }
                
            }
        }
    }
}
