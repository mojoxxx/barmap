//
//  AlertMessage.swift
//  BarMap
//
//  Created by Mojo on 2019/7/23.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation
import UIKit

class AlertMessage{
    static let shared = AlertMessage()
    func alert(title: String?, message: String,vc: UIViewController){
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        controller.addAction(okAction)
        vc.present(controller, animated: true, completion: nil)
    }
}
