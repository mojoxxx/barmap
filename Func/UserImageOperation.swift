//
//  UserImageOperation.swift
//  BarMap
//
//  Created by Mojo on 2019/7/11.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation
import UIKit

class UserImageOperation : Operation{
    var url : URL!
    var tableView : UITableView!
    var indexPath : IndexPath!
    let editImage = EditImage.shared
    
    init(url : URL, tableView : UITableView, indexPath : IndexPath) {
        self.url = url
        self.tableView = tableView
        self.indexPath = indexPath
        super.init()
    }
    
    //main方式,預設情況下是執行在背景執行緒
    override func main() {
        
        if self.isCancelled{
            return
        }
        
        
        if let imageData = try? Data(contentsOf: url){
            DispatchQueue.main.async {
                //下載完時，判斷該位置的cell是否在畫面上
                if let cell = self.tableView.cellForRow(at: self.indexPath) as? CommentTableViewCell{
                    //如果該位置在停留的畫面上，則cell會有值，否則為空值
                    
                    guard let image = UIImage(data: imageData) else{
                        return
                    }
                    cell.userImageView?.image = self.editImage.thumbnailImage(image: image,width :40,height :40,circle :true,background: false)
//                    cell.setNeedsLayout()//通知Layout重新排列,coustomCell不會有作用要另外寫
                }
                
            }
        }
    }
}
