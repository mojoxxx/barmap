//
//  ServerConnect.swift
//  BarMap
//
//  Created by Mojo on 2019/6/15.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation

class ServerConnect{
    static let shared = ServerConnect()
    let domain = "http://35.201.152.195:7777/"
//    private init(){
//        sentJsonToServer(urlString: "aaa", encodedData: Data()) { (data) in
//
//        }
//
//
//    }
    func sentJsonToServer(urlString: String,encodedData: Data, completionHandler : @escaping (Data) -> Void ){
        guard let url = URL(string: urlString) else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accpet")
        
//        print(String(data: encodedData, encoding: .utf8)!) //<- Looks as intended
        
        request.httpBody = encodedData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in  
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            completionHandler(data)
            
            /* test
            guard let jsonData = String(data: data, encoding: .utf8) else{
                return
            }
            print(jsonData)
            */
        }
        task.resume()
    }
    
    func uploadImage(uploadUrl:String ,imgData:Data){
        let url = URL(string: uploadUrl)
        var request = URLRequest(url: url!, cachePolicy: .reloadIgnoringCacheData)
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        let uploadTask = session.uploadTask(with: request, from: imgData) {
            (data, response, error) -> Void in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            do{
                guard let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any] else{
                    return
                }
                print("status = \(json["status"]!)")
                
            }catch{
                print("Return data=\(String(describing: String(data: data, encoding: .utf8)))")
                print("JSONSerialization error \(error)")
                return
            }
            print("uploadImage success")
        }
        uploadTask.resume()
    }
    
    func uploadStioreImage(uploadUrl:String ,imgData:Data, completionHandler : @escaping (Data) -> Void ){
        let url = URL(string: uploadUrl)
        var request = URLRequest(url: url!, cachePolicy: .reloadIgnoringCacheData)
        request.httpMethod = "POST"
        
        let session = URLSession.shared
        
        //        let imgData = try! Data(contentsOf: fileUrl)
        
        let uploadTask = session.uploadTask(with: request, from: imgData) {
            (data, response, error) -> Void in
            guard let data = data, error == nil else {
                print(error?.localizedDescription ?? "No data")
                return
            }
            completionHandler(data)
        }
        uploadTask.resume()
    }
    

}
