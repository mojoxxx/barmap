//
//  UserTracking.swift
//  BarMap
//
//  Created by Mojo on 2019/7/2.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation

class UserTracking: Codable,Equatable{
    
    static func == (lhs: UserTracking, rhs: UserTracking) -> Bool {
        return lhs === rhs
    }
    
    var userTrackingNo : String = UUID().uuidString
    var userAccount : String?
    var storeID : String?
}
