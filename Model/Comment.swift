//
//  Comment.swift
//  BarMap
//
//  Created by Mojo on 2019/7/11.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation

class Comment: Codable{
    var commentNo : String = UUID().uuidString
    var userAccount : String?
    var text : String?
    var fraction : Double?
    var storeID  : String?
    var userName : String?
    
}
