//
//  StoreImage.swift
//  BarMap
//
//  Created by Mojo on 2019/6/25.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation

class StoreImage: Codable{
    let imageName : String
    let storeID : String
    
    init(imageName: String,storeID: String){
        self.imageName = imageName
        self.storeID = storeID
    }
}
