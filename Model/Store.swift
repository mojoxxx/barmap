//
//  Store.swift
//  BarMap
//
//  Created by Mojo on 2019/6/24.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation

class Store: Codable,Equatable{
    
    static func == (lhs: Store, rhs: Store) -> Bool {
        return lhs === rhs
    }
    
    var storeID : String = UUID().uuidString
    var storeName : String?
    var longitude : Double?
    var latitude : Double?
    var address  : String?
    var tel  : String?
    var avgFraction : Double?
    
}
