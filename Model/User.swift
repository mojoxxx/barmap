//
//  File.swift
//  BarMap
//
//  Created by Mojo on 2019/6/14.
//  Copyright © 2019 Mojo. All rights reserved.
//

import Foundation

class User: Codable{
    var userAccount : String = ""
    var userName : String = ""
    var userImageName : String = ""
}
