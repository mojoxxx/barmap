//
//  addStoreViewController.swift
//  BarMap
//
//  Created by Mojo on 2019/6/23.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Photos
import MobileCoreServices


protocol addStoreViewControllerDelegate : class {
    func didFinishAddStore(store : Store)
    func didNothingAddStore()
}

class AddStoreViewController: UIViewController {
    
    let serverConnect = ServerConnect.shared
    let editImage = EditImage.shared
    let alert = AlertMessage.shared
    let addressAndLocationTransform = AddressAndLocationTransform.shared
    var manger = CLLocationManager()
    
    @IBOutlet weak var addMapView: MKMapView!
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var addressText: UITextField!
    @IBOutlet weak var telText: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var currentStore : Store!
    weak var delegate : addStoreViewControllerDelegate?
    var alreadyHaveAddress = false
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        addMapView.delegate = self
        addMapView.showsUserLocation = true   //顯示user位置
        addMapView.userTrackingMode = .follow  //隨著user移動
        guard CLLocationManager.locationServicesEnabled() else {
            return
        }
        
        //多工背景
        //manger.allowsBackgroundLocationUpdates = true
        
        //start to update location
        manger.delegate = self
        manger.desiredAccuracy = kCLLocationAccuracyBest//設定為最佳精度
        manger.activityType = .automotiveNavigation
        manger.startUpdatingLocation() //開始update user位置
        
        self.nameText.keyboardAppearance = .dark
        self.addressText.keyboardAppearance = .dark
        self.telText.keyboardAppearance = .dark
        self.telText.keyboardType = .numberPad
        
        let item = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(self.back))
        item.image = UIImage(named: "Back")
        self.navigationItem.leftBarButtonItem = item
        
        
        setMapview()
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.delegate?.didNothingAddStore()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func addressEditingDidEnd(_ sender: Any) {
        print("addressEditingDidEnd")
        guard let address = self.addressText.text,address != "" else{
            return
        }
        addressAndLocationTransform.addressToLocation(address: address) { (coordinate) in
            guard self.checkSearchArea(latitude: coordinate.latitude,longitude: coordinate.longitude) else{
                DispatchQueue.main.async {
                   self.addressText.text = ""
                }
                return
            }
            self.checkAlreadyHaveAddress(address: address, latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
    }
    
    
    @IBAction func searchButton(_ sender: Any) {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.searchBar.barStyle = UIBarStyle.black
        searchController.searchBar.prompt  = "請搜尋店家或地址"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.keyboardAppearance = .dark
        searchController.searchBar.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        var rect = searchController.searchBar.frame;
        rect.size.height = 75;
        searchController.searchBar.frame = rect
        searchController.searchBar.searchFieldBackgroundPositionAdjustment = UIOffset(horizontal: 0, vertical: 20);
        present(searchController, animated: true, completion: nil)
    }
    
    @IBAction func camera(_ sender: Any) {
        // Ask user's permission
        PHPhotoLibrary.requestAuthorization { (status) in
            NSLog("PHPhotoLibrary auth status: \(status)")
        }
        
        let sourceType:UIImagePickerController.SourceType
        sourceType = .camera
        
        launchImagePicker(type: sourceType)
    }
    
    @IBAction func photo(_ sender: Any) {
        // Ask user's permission
        PHPhotoLibrary.requestAuthorization { (status) in
            NSLog("PHPhotoLibrary auth status: \(status)")
        }
        
        let sourceType:UIImagePickerController.SourceType
        sourceType = .photoLibrary
        
        launchImagePicker(type: sourceType)
    }
    
    func launchImagePicker(type:UIImagePickerController.SourceType) {
        
        guard UIImagePickerController.isSourceTypeAvailable(type) else {
            NSLog("Invalid source type.")
            return
        }
        
        let picker = UIImagePickerController()
        picker.sourceType = type
        picker.allowsEditing = type == .photoLibrary ? true : false
        picker.delegate = self
        
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func done(_ sender: Any) {
        guard checkText() else{
            return
        }
        
        guard !self.alreadyHaveAddress else{
            self.alert.alert(title: nil, message: "此地址已有店家\n請檢查地址是否正確", vc: self)
            return
        }
        self.currentStore.storeName = self.nameText.text
        self.currentStore.address = self.addressText.text
        self.currentStore.tel = self.telText.text
        self.currentStore.avgFraction = 0 //設定初值
         
        if let image = self.imageView.image{
            let fileName = String(format: "%@.jpg", UUID().uuidString)
            let storeImage = StoreImage(imageName: fileName, storeID: self.currentStore.storeID)
            
            
//            guard let thumbnailImage = self.editImage.thumbnailImage(image: image,width :1024,height :1024,circle :false),
//                let data = thumbnailImage.jpegData(compressionQuality: 0.7),
            guard let data = image.jpegData(compressionQuality: 0.7),
                let encodedData = try? JSONEncoder().encode(storeImage) else{
                    assertionFailure("Convert image or encode storeImage fail.")
                    return
            }
            self.serverConnect.uploadImage(uploadUrl: "\(serverConnect.domain)uploadStoreImage.php?fileName=\(fileName)", imgData: data)
            self.serverConnect.sentJsonToServer(urlString: "\(serverConnect.domain)insertStoreImage.php", encodedData: encodedData){ (data) in
                guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                    else{
                        return
                }
                print("uploadStoreImage \(String(describing: json["status"]))")
            }
        }
        
        self.delegate?.didFinishAddStore(store: self.currentStore)
        self.navigationController?.popViewController(animated: true)
    }
    
    func checkText()-> Bool{
        if self.nameText.text?.isEmpty == true || self.addressText.text?.isEmpty == true {
            self.alert.alert(title: nil, message: "請輸入店名和地址", vc: self)
            return false
        }else if checkTel(){
            self.alert.alert(title: nil, message: "請檢查電話號碼\n 請輸入10碼", vc: self)
            return false
        }else{
            return true
        }
    }
    
    func checkTel()-> Bool{
        let PHONE_REGEX = "^\\d{10}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: self.telText.text) || self.telText.text == ""
        return !result
    }
    
    func checkAlreadyHaveAddress(address: String,latitude: Double, longitude: Double){
        let store = Store()
        store.address = address
        store.latitude = latitude
        store.longitude = longitude
        
        if let encodedData = try? JSONEncoder().encode(store){
            self.serverConnect.sentJsonToServer(urlString:
            "\(serverConnect.domain)checkAlreadyHaveAddress.php", encodedData: encodedData){ (data) in

                guard let resultCount = String(data: data, encoding: .utf8) else{
                    return
                }
                print("checkAlreadyHaveAddress OK,resultCount = \(resultCount)")
                if resultCount == "0" {
                    self.alreadyHaveAddress = false
                }else{
                    self.alreadyHaveAddress = true
                }
            }
            
        }
    }
    
    func checkSearchArea(latitude: CLLocationDegrees,longitude: CLLocationDegrees)-> Bool{
//        if latitude > 25.287868 || latitude < 24.586904 || longitude > 121.971207 || longitude < 120.981344{
//            self.alert.alert(title: nil, message: "目前僅開放大台北大台地區，\n敬請期待其他地區☺️", vc: self)
//            return false
//        }else{
//            return true
//        }
        return true
    }
    
}

//MARK:UITextFieldDelegate
extension AddStoreViewController: UITextFieldDelegate{
    
    //點擊空白處關閉鍵盤
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

//MARK:UIImagePickerControllerDelegate,UINavigationControllerDelegate
extension AddStoreViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
//        var image : UIImage?
//        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
//            image = editedImage
//        }else{
//            image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
//        }
        
        let image = info[UIImagePickerController.InfoKey.editedImage] != nil ?
            info[UIImagePickerController.InfoKey.editedImage] as! UIImage :
            info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        self.imageView.image = image
        self.dismiss(animated: true, completion: nil)//close imagePickerController
        
    }
}

//MARK:UISearchBarDelegate
extension AddStoreViewController : UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = .gray
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
        self.view.addSubview(activityIndicator)
        
        //hide search bar
        searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
        
        //create the search request
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchBar.text
        
        let activeSearch = MKLocalSearch(request: searchRequest)
        
        activeSearch.start { (response, error) in
            
            activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            guard response != nil,error == nil else{
                print("error \(String(describing: error))")
                self.alert.alert(title: "錯誤", message: "找不到此地點", vc: self)
                return
            }
            //Remove annotations
            let annotations = self.addMapView.annotations
            self.addMapView.removeAnnotations(annotations)
            
            //Getting data
            guard let latitude = response?.boundingRegion.center.latitude else{
                assertionFailure("Fail to get latitude.")
                return
            }
            guard let longitude = response?.boundingRegion.center.longitude else{
                assertionFailure("Fail to get longitude.")
                return
            }
            
            guard self.checkSearchArea(latitude: latitude,longitude: longitude) else{
                return
            }
            
            //get phone,name,address
            if let mapItem = response?.mapItems[0] {
                if var phone = mapItem.phoneNumber {
                    phone = phone.replacingOccurrences(of: "+886 ", with: "0")
                    phone = phone.replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
//                    phone = phone.replacingOccurrences(of: "-", with: "")
                    self.telText.text = phone
                }
                if let name = mapItem.name{
                    self.nameText.text = name
                }else{
                    self.nameText.text = searchBar.text
                }
            }
            
            DispatchQueue.global().async {
                self.addressAndLocationTransform.locationAddress(latitude: latitude, longitude: longitude, completionHandler: { (address) in
                    DispatchQueue.main.async {
                        self.addressText.text = address
                        self.checkAlreadyHaveAddress(address: address, latitude: latitude, longitude: longitude)
                    }
                })
            }
            
            
            //Create annotation
            let annotation = MKPointAnnotation()
            annotation.title = searchBar.text
            annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
            self.addMapView.addAnnotation(annotation)
            
            //Zooming in on annotation
            let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region = MKCoordinateRegion(center: coordinate, span: span)
            self.addMapView.setRegion(region, animated: true)
            
        }
        
    }
}

//MARK: CLLocationManagerDelegate
extension AddStoreViewController : CLLocationManagerDelegate{
    //    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    //        guard let coordinate = locations.last?.coordinate else {
    //            assertionFailure("Fail to go location")
    //            return
    //        }
    //        print("lat: \(coordinate.latitude), lon: \(coordinate.longitude)")
    //    }
}

//MARK:MKMapViewDelegate
extension AddStoreViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView") as? MKMarkerAnnotationView
        
        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
            
        }
        annotationView?.markerTintColor = UIColor.init(displayP3Red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
        annotationView?.glyphImage = UIImage(named: "BarIcon.png")
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    
}

//MARK: UIGestureRecognizerDelegate
extension AddStoreViewController: UIGestureRecognizerDelegate {
    
    func setMapview(){
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureReconizer:)))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = false
        lpgr.delegate = self
        self.addMapView.addGestureRecognizer(lpgr)
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            return
        }
        
        if gestureReconizer.state != UIGestureRecognizer.State.began {
            
            let touchLocation = gestureReconizer.location(in: addMapView)
            let locationCoordinate = addMapView.convert(touchLocation,toCoordinateFrom: addMapView)
            let latitude = Double(round(1000000*locationCoordinate.latitude)/1000000)
            let longitude = Double(round(1000000*locationCoordinate.longitude)/1000000)
            print("Tapped at lat: \(latitude) long: \(longitude)")
            
            guard self.checkSearchArea(latitude: latitude,longitude: longitude) else{
                return
            }
            
            self.addressAndLocationTransform.locationAddress(latitude: latitude, longitude: longitude) { (address) in
                self.addressText.text = address
                let allAnnotations = self.addMapView.annotations
                self.addMapView.removeAnnotations(allAnnotations)
                
                if latitude != 0.0 {
                    
                    let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = coordinate
                    annotation.title = address
                    
                    //delay 0.5s
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5){
                        self.addMapView.addAnnotation(annotation)
                    }
                }
                
                self.checkAlreadyHaveAddress(address: address, latitude: latitude, longitude: longitude)
            }
            
            return
        }
    }
}
