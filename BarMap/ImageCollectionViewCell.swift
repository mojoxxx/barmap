//
//  ImageCollectionViewCell.swift
//  BarMap
//
//  Created by Mojo on 2019/7/5.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
