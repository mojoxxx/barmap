//
//  MapViewController.swift
//  BarMap
//
//  Created by Mojo on 2019/6/14.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import MessageUI
import GoogleMobileAds
import Network

class MapViewController: UIViewController{
    
    let serverConnect = ServerConnect.shared
    let editImage = EditImage.shared
    let addressAndLocationTransform = AddressAndLocationTransform.shared
    let alert = AlertMessage.shared
    let queue = OperationQueue()
    var manger = CLLocationManager()
    let monitor = NWPathMonitor()
    
    
    var user = User()
    var stores : [Store] = []
    var storeImages : [StoreImage] = []
    var photos : [UIImage] = []
    var userTrackings : [UserTracking] = []
    var tempAnnotation : MKAnnotation?
    var newAnnotation : MKAnnotation?
    var tempUserTracking = UserTracking()
    var tempStore = Store()
    var isUserTracking = false
    var backFrom = ""
    var isFollow = true
    var latitude: CLLocationDegrees = 25.0334115
    var longitude: CLLocationDegrees = 121.5177452
    
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var mainMapView: MKMapView!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    @IBOutlet weak var storeNameLabel: UILabel!
    @IBOutlet weak var storeAddressLabel: UILabel!
    @IBOutlet weak var telLabel: UILabel!
    @IBOutlet weak var farctionLabel: UILabel!
    @IBOutlet weak var favoriteBtn: UIBarButtonItem!
    @IBOutlet weak var userTrackingMode: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint?
    var bannerView : GADBannerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        guard UserDefaults.standard.string(forKey: "userAccount") != nil else{
            if let signInViewController = storyboard?.instantiateViewController(withIdentifier: "SignInVC"){
                self.present(signInViewController, animated: true, completion: nil)
            }
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        print("viewWillAppear")
        guard self.tempAnnotation == nil,self.backFrom == "" else{
            return
        }
        
        guard let userAccount = UserDefaults.standard.string(forKey: "userAccount") else{
            return
        }
        self.user.userAccount = userAccount
        print("userAccount=\(userAccount)")
        
        ///監聽是否重新進入程序.
//        NotificationCenter.default.addObserver(self, selector: #selector(self.reload), name: UIApplication.willEnterForegroundNotification, object: nil)
        
        self.detailView.alpha = 0.9
        self.view.sendSubviewToBack(detailView)
        self.mainMapView.delegate = self
        self.mainMapView.showsUserLocation = true   //顯示user位置
        self.mainMapView.userTrackingMode = .follow  //隨著user移動
        if let image = UIImage(named: "follow.png") {
            self.userTrackingMode.setImage(editImage.thumbnailImage(image: image, width: 40, height: 40, circle: true,background: false), for: .normal)
        }
        self.isFollow = true
        guard CLLocationManager.locationServicesEnabled() else {
            return
        }
        // Ask user's permission
        self.manger.requestWhenInUseAuthorization()
        //多工背景
        //self.manger.allowsBackgroundLocationUpdates = true
        
        //start to update location
        self.manger.delegate = self
        self.manger.desiredAccuracy = kCLLocationAccuracyBest//設定為最佳精度
        self.manger.activityType = .automotiveNavigation
        self.manger.startUpdatingLocation() //開始update user位置
        
        //test
//        let addressAndLocationTransform = AddressAndLocationTransform.shared
//        print(addressAndLocationTransform.addressToLocation(address:"台北市信義區嘉興街36號"))
//        addressAndLocationTransform.locationAddress(latitude: 25.036125, longitude: 121.547541) { (address) in
//            print(address)
//        }
        
        //imageCollectionView
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: self.imageCollectionView.frame.size.height, height: self.imageCollectionView.frame.size.height)
        layout.scrollDirection = UICollectionView.ScrollDirection.horizontal 
        layout.minimumLineSpacing = CGFloat(integerLiteral: 0)
        layout.minimumInteritemSpacing = CGFloat(integerLiteral: 0)
        self.imageCollectionView.collectionViewLayout = layout
        
        //手勢
        self.setSwipeDownRecognizer()
        
        //Tel
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.callTel))
//        telLabel.isUserInteractionEnabled = true
//        telLabel.addGestureRecognizer(tap)

        
        
        //AD
        self.bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        self.bannerView.translatesAutoresizingMaskIntoConstraints = false
        self.bannerView.adUnitID = "ca-app-pub-4348354487644961/4047987302" //廣告單元ID
        self.bannerView.rootViewController = self
        self.bannerView.load(GADRequest())
        self.bannerView.delegate = self
        
        //確認網路
        self.monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                print("connected")
            } else {
                self.alert.alert(title: nil, message: "偵測不到網路\n請確認網路連線\n確認後\n請按首頁上方重整按鈕\n重新載入", vc: self)
            }
        }
        self.monitor.start(queue: DispatchQueue.global())

//        guard Reachability.isConnectedToNetwork() else{
//            alert.alert(title: nil, message: "請確認網路連線\n確認後請按上方重整按鈕重新載入", vc: self)
//            return
//        }
        
        // Add Annotation to MapView
        self.queryUserTracking()
        self.queryStores()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print("viewDidAppear")
        guard let annotation = self.tempAnnotation else{
            return
        }
        if self.backFrom == "Comment"{
            DispatchQueue.main.async {
                self.mainMapView.removeAnnotation(annotation)
                self.addAnnotationtoMapView(store: self.tempStore)
                guard let newAnnotation = self.newAnnotation else{
                    return
                }
                self.mainMapView.selectAnnotation(newAnnotation, animated: true)
            }
        }else if self.backFrom == "EditImage"{
            self.mainMapView.deselectAnnotation(annotation, animated: true)
            self.mainMapView.selectAnnotation(annotation, animated: true)
        }
    }
    
     @objc @IBAction func reload(_ sender: Any) {
        print("reload")
        
        self.returnToTheOriginal()

        //reload
        self.queryUserTracking()
        self.queryStores()
    }
    
    @IBAction func logOut(_ sender: Any) {
        let controller = UIAlertController(title: nil, message: "確定登出？", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "CANCEL", style: .default, handler: nil)
        controller.addAction(cancelAction)
        
        let okAction = UIAlertAction(title: "OK", style: .default){ (_) in
            self.returnToTheOriginal()
            
            if let signInViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignInVC"){
                self.present(signInViewController, animated: true, completion: nil)
            }
        }
        controller.addAction(okAction)
        
        present(controller, animated: true, completion: nil)

        
    }
    
    func returnToTheOriginal(){
        
        //Remove annotations
        let annotations = self.mainMapView.annotations
        self.mainMapView.removeAnnotations(annotations)
        
        self.stores = []
        self.storeImages = []
        self.photos = []
        self.userTrackings = []
        self.tempAnnotation = nil
        self.newAnnotation = nil
        self.tempUserTracking = UserTracking()
        self.tempStore = Store()
        self.backFrom = ""
    }
    
    @IBAction func searchButton(_ sender: Any) {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.searchBar.barStyle = UIBarStyle.black
        searchController.searchBar.prompt  = "尋找店家"
        searchController.searchBar.sizeToFit()
        searchController.searchBar.keyboardAppearance = .dark
        searchController.searchBar.tintColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        var rect = searchController.searchBar.frame;
        rect.size.height = 75;
        searchController.searchBar.frame = rect
        searchController.searchBar.searchFieldBackgroundPositionAdjustment = UIOffset(horizontal: 0, vertical: 20);
        present(searchController, animated: true, completion: nil)
    }
    
    @IBAction func trackingModePressed(_ sender: UIButton) {
        if self.isFollow {
            self.isFollow = false
            if let image = UIImage(named: "followWithHeading.png") {
                self.userTrackingMode.setImage(editImage.thumbnailImage(image: image, width: 40, height: 40, circle: true,background: false), for: .normal)
                self.mainMapView.userTrackingMode = .followWithHeading
            }
        }else{
            self.isFollow = true
            if let image = UIImage(named: "follow.png") {
                self.userTrackingMode.setImage(editImage.thumbnailImage(image: image, width: 40, height: 40, circle: true,background: false), for: .normal)
                self.mainMapView.userTrackingMode = .follow
            }
            
        }
    }
    
    @IBAction func mapTypeChanged(_ sender: UISegmentedControl) {
        let targetIndex = sender.selectedSegmentIndex
        
        switch targetIndex {
            
        case 0:
            mainMapView.mapType = .standard
        case 1:
            mainMapView.mapType = .hybrid
//        case 2:
//            if #available(iOS 9.0, *) {
//                mainMapView.mapType = .satelliteFlyover
//                let coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude)
//                let camera = MKMapCamera(lookingAtCenter: coordinate, fromDistance: 700, pitch: 65, heading: 0)
//                mainMapView.camera = camera
//            } else {
//                mainMapView.mapType = .standard
//            }
        default:
            mainMapView.mapType = .standard
        }
    }
    
    
    func queryUserTracking(){
        guard let encodedData = try? JSONEncoder().encode(self.user) else{
             assertionFailure("Fail to encode userAccount.")
            return
        }
        
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)queryUserTracking.php", encodedData: encodedData){ (data) in
            do {
                self.userTrackings = try JSONDecoder().decode([UserTracking].self, from: data)
                print("queryUserTracking OK")
            }catch{
                print("error while parsing json \(error)")
            }
        }
        
    }
    
    func queryStores(){
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)queryStore.php", encodedData: Data()){ (data) in
            do {
                self.stores = try JSONDecoder().decode([Store].self, from: data)
                print("queryStore OK")
                DispatchQueue.main.async {
                    for store in self.stores{
                        self.addAnnotationtoMapView(store:store)
                    }
                }
            }catch{
                print("error while parsing json \(error)")
            }
        }
        
    }
    
    //MARK load imageData
    func queryStoreImage(storeID:String){
        let initData = ["storeID":storeID]
        guard let encodedData = try? JSONEncoder().encode(initData) else{
            assertionFailure("FJSONEncoder Fail.")
            return
        }
        
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)queryStoreImage.php", encodedData: encodedData){ (data) in
            do {
                self.storeImages = try JSONDecoder().decode([StoreImage].self, from: data)
                if self.storeImages.isEmpty{
                    let storeImage = StoreImage(imageName: "add.png",storeID: "noID")
                    self.storeImages.append(storeImage)
                    print("queryStoreImage isEmpty")
                }else{
                   print("queryStoreImage OK")
                }
                
                DispatchQueue.main.async {
                    self.imageCollectionView.reloadData()
                }
            }catch{
                print("error while parsing json \(error)")
            }
        }
    }
    
    
    
    func addAnnotationtoMapView(store:Store){
        let storeID = store.storeID
        guard let latitude = store.latitude,let longitude = store.longitude,
            let avgFraction = store.avgFraction else{
                assertionFailure("Fail to get StoreData.")
                return
        }
        
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let annotation = MKPointAnnotation()
        annotation.coordinate =  coordinate
        annotation.title = storeID
        
        switch avgFraction {
        case 4.6...5:
            annotation.subtitle = "S"
        case 4.3...4.5:
            annotation.subtitle = "A"
        case 4.0...4.2:
            annotation.subtitle = "B"
        case 3.6...3.9:
            annotation.subtitle = "C"
        case 0...3.5:
            annotation.subtitle = "D"
        default:
            annotation.subtitle = "E"
        }
        
        self.mainMapView.addAnnotation(annotation)
        if self.backFrom == "Comment" || self.backFrom == "AddStore"{
            self.newAnnotation = annotation
        }
    }
    
//    @objc func callTel(){
//        print("callTel")
//        if (UIApplication.shared.canOpenURL(URL(string:"tel://")!)) {
//            guard let tel = self.telLabel.text ,let telURL = URL(string: "tel://\(tel))") else { return }
//            if #available(iOS 10.0, *) {
//                UIApplication.shared.open(telURL, options: [:], completionHandler: nil)
//            } else {
//                UIApplication.shared.openURL(telURL)
//            }
//        }
//    }
    @IBAction func callTel(_ sender: Any) {
        print("callTel")
        if (UIApplication.shared.canOpenURL(URL(string:"tel://")!)) {
            guard self.telLabel.text != "",let tel = self.telLabel.text ,let telURL = URL(string: "tel://\(tel))") else { return }
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(telURL, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(telURL)
            }
        }
    }
    
    @IBAction func routeBtn(_ sender: Any) {
        print("routeBtn")
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            guard let address = self.storeAddressLabel.text else{
                assertionFailure("Fail to get storeAddress.")
                return
            }
            let urlString = "comgooglemaps://?daddr=\(address)&directionsmode=walking"
            guard let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!) else{
                assertionFailure("Fail to get comgooglemaps url.")
                return
            }
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            
        } else {
            guard let sourceCoordinate = self.tempAnnotation?.coordinate else {
                assertionFailure("Fail to get sourceCoordinate.")
                return
            }
            let sourcePlace = MKPlacemark(coordinate: sourceCoordinate, addressDictionary: nil)
            let targetMapItem = MKMapItem(placemark: sourcePlace)
            let options = [MKLaunchOptionsDirectionsModeKey:MKLaunchOptionsDirectionsModeWalking]
            targetMapItem.openInMaps(launchOptions: options)
        }
        
    }
    
    @IBAction func favoriteBtn(_ sender: Any) {
        if self.isUserTracking{
            self.favoriteBtn.image = UIImage(named: "UnFavorite")
            //delete
            if let encodedData = try? JSONEncoder().encode(self.tempUserTracking){
                self.serverConnect.sentJsonToServer(urlString:
                "\(serverConnect.domain)deleteUserTracking.php", encodedData: encodedData){ (data) in
                    guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                        else{
                            assertionFailure("Fail to get jsonData.")
                            return
                    }
                    print("deleteUserTracking \(String(describing: json["status"]))")
                }
                if let index = self.userTrackings.firstIndex(of: self.tempUserTracking){
                    self.userTrackings.remove(at: index)
                }
                //Update map
                DispatchQueue.main.async {
                    if let annotation = self.tempAnnotation{
                        self.mainMapView.removeAnnotation(annotation)
                        self.mainMapView.addAnnotation(annotation)
                        self.mainMapView.selectAnnotation(annotation, animated: true)
                    }
                }
            }
        }else{
            self.favoriteBtn.image = UIImage(named: "Favorite")
            //insert
            let newUserTracking = UserTracking()
            newUserTracking.userAccount = self.user.userAccount
            guard let title = self.tempAnnotation?.title else{
                assertionFailure("Fail to get tempAnnotation?.title.")
                return
            }
            newUserTracking.storeID = title
            if let encodedData = try? JSONEncoder().encode(newUserTracking){
                self.serverConnect.sentJsonToServer(urlString:
                "\(serverConnect.domain)insertUserTracking.php", encodedData: encodedData){ (data) in
                    guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                        else{
                            assertionFailure("Fail to get jsonData.")
                            return
                    }
                    print("insertUserTracking \(String(describing: json["status"]))")
                }
                self.userTrackings.insert(newUserTracking, at:self.userTrackings.count)
                //Update map
                DispatchQueue.main.async {
                    if let annotation = self.tempAnnotation{
                        self.mainMapView.removeAnnotation(annotation)
                        self.mainMapView.addAnnotation(annotation)
                        self.mainMapView.selectAnnotation(annotation, animated: true)
                    }
                }
            }
        }
    }
    
    
    @IBAction func sendMail(_ sender: Any) {
        if MFMailComposeViewController.canSendMail(){
            let mailController = MFMailComposeViewController()
            mailController.mailComposeDelegate = self
            mailController.setSubject("回報問題")
            mailController.setToRecipients(["kiziko76121@gmail.com"])
            guard let storeName = self.storeNameLabel.text else{
                return
            }
            let body = "StoreName:\(storeName)\nStoreID:\(self.tempStore.storeID)\n問題描述:"
            mailController.setMessageBody(body, isHTML: false)
            self.present(mailController, animated: true, completion: nil)
            
        }
    }
    
    
    func checkIsUserTracking(value: String)-> Bool{
        for userTracking in self.userTrackings{
            if userTracking.storeID == value{
                self.tempUserTracking = userTracking
                self.isUserTracking = true
                return true
            }
        }
        self.isUserTracking = false
        return false
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.backFrom = ""
        self.newAnnotation = nil
        if segue.identifier == "addStoreSegue"{
            if self.tempAnnotation != nil{
                self.mainMapView.deselectAnnotation(self.tempAnnotation, animated: true)
            }
            let store = Store()
            let addStoreVC = segue.destination as! AddStoreViewController
            addStoreVC.currentStore = store
            addStoreVC.delegate = self
        }else if segue.identifier == "imageSegue"{
            let bigImageVC = segue.destination as! BigImageViewController
            if self.storeImages.first?.storeID != "noID"{
                bigImageVC.storeImages = self.storeImages
            }
            bigImageVC.delegate = self
            if let storeID = self.tempAnnotation?.title{
                bigImageVC.storeID = storeID
            }
        }else if segue.identifier == "commentSegue"{
            let commentVC = segue.destination as! CommentViewController
            commentVC.delegate = self
            commentVC.currentStore = self.tempStore
        }
    }
    
    
}

extension MapViewController : UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.style = .gray
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
        self.view.addSubview(activityIndicator)
        
        //hide search bar
        searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
        
        //create the search request
        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchBar.text
        
        let activeSearch = MKLocalSearch(request: searchRequest)
        
        activeSearch.start { (response, error) in
            
            activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            guard response != nil,error == nil else{
                print("error \(String(describing: error))")
                self.alert.alert(title: "錯誤", message: "找不到此地點", vc: self)
                return
            }
            
            //Getting data
            guard let latitude = response?.boundingRegion.center.latitude else{
                assertionFailure("Fail to get latitude.")
                return
            }
            guard let longitude = response?.boundingRegion.center.longitude else{
                assertionFailure("Fail to get longitude.")
                return
            }
            //Create annotation
//            let annotation = MKPointAnnotation()
//            annotation.title = searchBar.text
//            annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
//            self.addMapView.addAnnotation(annotation)
            
            //Zooming in on annotation
            let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude, longitude)
            let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            let region = MKCoordinateRegion(center: coordinate, span: span)
            self.mainMapView.setRegion(region, animated: true)
            
            let annotations = self.mainMapView.annotations
            print("latitude=\(latitude),longitude=\(longitude)")
            for annotation in annotations {
                if round(1000*latitude)/1000 == round(1000*annotation.coordinate.latitude)/1000 && round(1000*longitude)/1000 == round(1000*annotation.coordinate.longitude)/1000{
                     self.mainMapView.selectAnnotation(annotation, animated: true)
                    break
                }
            }
            
        }
    }
}

//MARK: CLLocationManagerDelegate
extension MapViewController : CLLocationManagerDelegate{
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            guard let coordinate = locations.last?.coordinate else {
                return
            }
            self.latitude = coordinate.latitude
            self.longitude = coordinate.longitude
        }
}

//MARK:MKMapViewDelegate
extension MapViewController: MKMapViewDelegate{
    
    //build annotation
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation
        {
            return nil
        }
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView") as? MKMarkerAnnotationView
        
        if annotationView == nil {
            annotationView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView")
            
        }
        
        if let subtitle = annotation.subtitle, subtitle != nil {
            //            annotationView?.image = UIImage(named: subtitle!)
            
            switch subtitle {
            case "S":
                annotationView?.markerTintColor = UIColor.black
                annotationView?.displayPriority = .defaultHigh //優先權
            case "A":
                annotationView?.markerTintColor = UIColor.init(displayP3Red: 77/255, green: 77/255, blue: 77/255, alpha: 1)
            case "B":
                annotationView?.markerTintColor = UIColor.gray
            case "C":
                annotationView?.markerTintColor = UIColor.init(displayP3Red: 192/255, green: 192/255, blue: 192/255, alpha: 1)
            case "D":
                annotationView?.markerTintColor = UIColor.init(displayP3Red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
            default:
                annotationView?.markerTintColor = UIColor.white
            }
        }
        
        var imageName = "BarIcon.png"
        for userTracking in self.userTrackings{
            if(userTracking.storeID == annotation.title){
                imageName = "Love.png"
                annotationView?.markerTintColor = UIColor.red
                annotationView?.displayPriority = .defaultHigh
                break
            }
        }
        
        annotationView?.glyphImage = UIImage(named: imageName)
        annotationView?.titleVisibility = .hidden
        annotationView?.subtitleVisibility = .hidden
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    //MARK: into Annotation
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView){
        guard view.annotation?.title != "My Location" else{
            return
        }
        self.view.bringSubviewToFront(self.detailView)
        let annotation = view.annotation
        self.tempAnnotation = annotation//暫存下來導航會用到
        guard let title = annotation?.title as? String else{
            assertionFailure("Fail to get annotation?.title.")
            return
        }
        
        //load images
        queryStoreImage(storeID:title)
        
        for store in self.stores{
            if store.storeID == title{
                self.tempStore = store
                self.storeNameLabel.text = store.storeName
                self.storeAddressLabel.text = store.address
                self.telLabel.text = store.tel
                guard let avgFraction = store.avgFraction else{
                    continue
                }
                guard avgFraction != 0 else{
                    self.farctionLabel.text = "-"
                    break
                }
                self.farctionLabel.text = String(avgFraction)
                break
            }
        }
        
        if checkIsUserTracking(value: title){
            self.favoriteBtn.image = UIImage(named: "Favorite")
        }else{
            self.favoriteBtn.image = UIImage(named: "UnFavorite")
        }
    }
    
    //leave Annotation
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView){
        self.photos = []
        self.tempAnnotation = nil
        self.view.sendSubviewToBack(detailView)
    }
    
    
}

//MARK: CommentViewControllerDelegate
extension MapViewController:CommentViewControllerDelegate{
    func didFinishComment(store: Store,isEdited: Bool) {
        print("didFinishEditComment")
        guard let index = self.stores.firstIndex(of: store),isEdited else{
            return
        }
        self.stores[index].avgFraction = store.avgFraction
        self.tempStore = store
        self.backFrom = "Comment"
    }
}

//MARK: BigImageViewControllerDelegate
extension MapViewController:BigImageViewControllerDelegate{
    func didFinishEditImage(isEdited: Bool) {
        print("didFinishEditImage")
        guard isEdited else{
           return
        }
        self.backFrom = "EditImage"
    }
}


//MARK: addStoreViewControllerDelegate
extension MapViewController: addStoreViewControllerDelegate{
    func didNothingAddStore() {
        print("didNothingAddStore")
        self.backFrom = "AddStore"
    }
    
    func didFinishAddStore(store: Store) {
        print("didFinishAddStore")
        self.backFrom = "AddStore"
        //Insert store
        guard let address = store.address else{
            assertionFailure("Fail to get store address.")
            return
        }
        
        //Addrrss -->Lat,Lon
        addressAndLocationTransform.addressToLocation(address: address, completionHandler: { (coordinate) in
            store.latitude = Double(coordinate.latitude)
            store.longitude = Double(coordinate.longitude)
            print("經緯度: \(coordinate.latitude),\(coordinate.longitude)")
            if let encodedData = try? JSONEncoder().encode(store){
                self.serverConnect.sentJsonToServer(urlString:
                "\(self.serverConnect.domain)insertStore.php", encodedData: encodedData){ (data) in
                    guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                        else{
                            return
                    }
                    print("insertStore \(String(describing: json["status"]))")
                }
                
                //Update map
                self.stores.insert(store, at:self.stores.count)
                DispatchQueue.main.async {
                    self.addAnnotationtoMapView(store:store)
                    guard let newAnnotation = self.newAnnotation else{
                        return
                    }
                    let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                    let region = MKCoordinateRegion(center: newAnnotation.coordinate, span: span)
                    self.mainMapView.setRegion(region, animated: true)
                    self.mainMapView.selectAnnotation(newAnnotation, animated: true)
                }
            }
        })
    }
    
}

//MARK:UICollectionViewDelegate
extension MapViewController:UICollectionViewDelegate{
    
}

//MARK:UICollectionViewDataSource
extension MapViewController:UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCollectionViewCell else{
            assertionFailure("Fail to get ImageCell.")
            return UICollectionViewCell()
        }
        cell.imageView.image = nil
        let imageName = self.storeImages[indexPath.row].imageName
        guard let url = URL(string: "\(serverConnect.domain)storeImage/\(imageName)") else{
            assertionFailure("Fail to get url.")
            return cell
        }
        
        let operation = ImageOperation(url: url, collectionView: collectionView, indexPath: indexPath)
        queue.addOperation(operation)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.queue.operations.filter { (op) -> Bool in
            let imageOperation = op as! ImageOperation
            if imageOperation.indexPath.compare(indexPath) == .orderedSame {
                return true
            }
            return false
            }.first?.cancel()
    }

}

//MARK:MFMailComposeViewControllerDelegate
extension MapViewController:MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: UIGestureRecognizerDelegate
extension MapViewController: UIGestureRecognizerDelegate {
    
    func setSwipeDownRecognizer(){
        let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDownClick(recognizer:)))
        swipeDownRecognizer.direction = .down
        swipeDownRecognizer.numberOfTouchesRequired = 1
        self.detailView.isUserInteractionEnabled = true
        self.detailView.addGestureRecognizer(swipeDownRecognizer)
    }
    
    @objc func swipeDownClick(recognizer:UISwipeGestureRecognizer)
    {
        print("swipeDownClick")
        guard let annotation = self.tempAnnotation else{
            return
        }
        self.mainMapView.deselectAnnotation(annotation, animated: true)
    }
}

//MARK: UIGestureRecognizerDelegate
extension MapViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        if self.bannerView.superview == nil {
            
            if self.topConstraint != nil {
                self.topConstraint?.isActive = false
            }
            self.view.addSubview(bannerView)
            
            //autolayout
            //廣告上緣－safeArea上緣
            self.bannerView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
            //廣告下緣－tableView上緣
            self.bannerView.bottomAnchor.constraint(equalTo: self.mainMapView.topAnchor, constant: 0).isActive = true
            //廣告左邊－controller'view 左邊
            self.bannerView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
            //廣告右邊－controller'view 右邊
            self.bannerView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
            
        }
    }
}
