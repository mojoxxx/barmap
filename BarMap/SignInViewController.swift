//
//  SignInViewController.swift
//  BarMap
//
//  Created by Mojo on 2019/6/13.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit
import GoogleSignIn
import GoogleAPIClientForREST
import GTMSessionFetcher
import FBSDKCoreKit
import FBSDKShareKit
import FBSDKLoginKit
import FBSDKMessengerShareKit


let googleClientID = "654144410249-9i2pv5mblk5jqemk6gl4kjfe9ktagl65.apps.googleusercontent.com"


class SignInViewController: UIViewController,GIDSignInDelegate,GIDSignInUIDelegate {

    var user = User()
    let serverConnect = ServerConnect.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if AccessToken.current != nil{
            LoginManager().logOut()
        }else if GIDSignIn.sharedInstance().hasAuthInKeychain(){
            GIDSignIn.sharedInstance()?.signOut()
        }
    }
    
    //MARK: google SignIn
    @IBAction func googleSignIn(_ sender: Any) {
        // Prepare for Google SignIn
        let signin = GIDSignIn.sharedInstance()
        signin?.delegate = self
        signin?.uiDelegate = self
        signin?.clientID = googleClientID
        
        //Check if we should login or not
        let authorizer = signin?.currentUser?.authentication?.fetcherAuthorizer()
        
        guard let canAuthorize = authorizer?.canAuthorize,canAuthorize == true else{
            //Can Authorize is nil. Do SignIn process.
            signin?.signIn()
            return
        }
    }
    
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("SignIn fail:\(error)")
            return
        }
        guard let email = user.profile.email else{
            assertionFailure("Fail to get email.")
            return
        }
        
        UserDefaults.standard.set(email, forKey: "userAccount")
        self.user.userAccount = email
        self.user.userName = user.profile.name
        self.user.userImageName = "\(self.user.userAccount).jpg"
        self.queryUser()
        if let picURL = user.profile.imageURL(withDimension: 400){
            self.saveUserImage(picURL:picURL ,userImageName:self.user.userImageName)
        }
        print("SignIn OK:\(email),\(user.profile.name ?? "n/a")")
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: facebook SignIn
    @IBAction func facebookSignIn(_ sender: Any) {
        Profile.enableUpdatesOnAccessTokenChange(true)//把通知打開
        let fbLoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile","email"], from: self, handler: { action, previewViewController in
            if action != nil {
                if let cancel = action?.isCancelled {
                    if !cancel {
                        print("SignIn OK")
                        self.getDetails()
                    }
                }
            } else {
                print("SignIn fail")
            }
        })
    }
       
        
    func getDetails(){
        
        //先判斷是否有token存在,有Token表示使用者有login
        if AccessToken.current != nil {
            let request = GraphRequest(graphPath: "me",parameters:["fields":"name, email , picture.width(400).height(400)"])
            request.start(completionHandler: { (connection, result, error) -> Void in
                let info = result as! Dictionary<String,AnyObject>
//                guard let email = info["email"] else{
//                    assertionFailure("Fail to get email.")
//                    return
//                }
                var email : String?
                if let infoEmail = info["email"] {
                    email = infoEmail as? String
                }else{
                    email = Profile.current?.userID
                }
                
                UserDefaults.standard.set(email, forKey: "userAccount")
                self.user.userAccount = email!
                self.user.userName = info["name"] as! String
                var picURL = ""
                if let photo = info["picture"] as? NSDictionary{
                    let data = photo["data"] as! NSDictionary
                    picURL = data["url"] as! String
                    self.user.userImageName = "\(self.user.userAccount).jpg"
                }
                self.queryUser()
                if let url = URL(string: picURL){
                    self.saveUserImage(picURL:url ,userImageName:self.user.userImageName)
                }
                print("email  = \(self.user.userAccount),name = \(self.user.userName)")
            })
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
    func queryUser(){
        if let encodedData = try? JSONEncoder().encode(self.user){
            
            self.serverConnect.sentJsonToServer(urlString:
            "\(serverConnect.domain)queryUser.php", encodedData: encodedData){ (data) in
                guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                    else{
                        return
                }
                print("queryUser \(String(describing: json["status"]))")
            }
        }
    }
    
    func insertUser(){

//        var serverConnect = ServerConnect()
//        if let encodedData = try? JSONEncoder().encode(self.userData){
//            guard let jsonData = serverConnect.sentJsonToServer(urlString: "\(serverConnect.domain)insertUser.php", encodedData: encodedData) else{
//                return
//            }
//            do {
//                let responseStatus = try JSONDecoder().decode(ResponseStatus.self, from: jsonData)
//                print("responseStatus=\(responseStatus.status)")
//            }catch{
//                print("error while parsing json \(error)")
//            }
//        }
    }
    
    func saveUserImage(picURL:URL,userImageName:String){
        DispatchQueue.global().async {
            let imgData = try! Data(contentsOf: picURL)
            self.serverConnect.uploadImage(uploadUrl: "\(self.serverConnect.domain)uploadUserImage.php?fileName=\(userImageName)", imgData: imgData)
        }
    }
    
}
