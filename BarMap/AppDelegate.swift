//
//  AppDelegate.swift
//  BarMap
//
//  Created by Mojo on 2019/6/13.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import Reachability
import IQKeyboardManagerSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reach : Reachability?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        IQKeyboardManager.shared.enable = true
//        self.reach = Reachability()
//        if self.reach?.connection == .wifi {
//            print("wifi")
//        }else if self.reach?.connection == .cellular {
//            print("4G")
//        }else if self.reach?.connection == .none {
//            print("沒有網路")
//        }
//        
//        do {
//            try self.reach?.startNotifier()//網路有變化送通知，名稱reachabilityChanged
//            NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.networkChange), name: Notification.Name.reachabilityChanged, object: nil)
//        }catch{
//            print("error \(error)")
//        }
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        
        return true
    }
    
    @objc func networkChange()  {
        
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
       AppEvents.activateApp()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//        guard let sourceApp = options[.sourceApplication] as? String else{
//            return false
//        }
//
//        let annotation = options[.annotation]
//
//        return GIDSignIn.sharedInstance().handle(url, sourceApplication: sourceApp, annotation: annotation)
        
//        return ApplicationDelegate.shared.application(app, open: url, options: options)
        print("applicationapplicationapplication")
        return ApplicationDelegate.shared.application(app, open: url, options: options) ||
        GIDSignIn.sharedInstance().handle(url, sourceApplication: options[.sourceApplication] as? String, annotation: options[.annotation])
    }


}

