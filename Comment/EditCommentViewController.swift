//
//  EditCommentViewController.swift
//  BarMap
//
//  Created by Mojo on 2019/7/10.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit
protocol EditCommentViewControllerDelegate {
    func didFinishEditComment(comment:Comment)
}

class EditCommentViewController: UIViewController {

    @IBOutlet var options: [UIButton]!
    @IBOutlet weak var textView: UITextView!
    var currentComment : Comment!
    var delegate : EditCommentViewControllerDelegate?
    var fraction : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.textView.keyboardAppearance = .dark
        
        let item = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(self.back))
        item.image = UIImage(named: "Back")
        self.navigationItem.leftBarButtonItem = item
        
        guard let currentFraction = self.currentComment.fraction
        else{
            return
        }
        self.fraction = "\(Int(currentFraction))"
        for option in options{
            if self.fraction == option.currentTitle{
                option.isHidden = false
                break
            }
        }
        self.textView.text = currentComment.text
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func optionPressed(_ sender: UIButton) {
        guard let fraction = sender.currentTitle else{
            return
        }
        self.fraction = fraction
        for option in options{
            if fraction != option.currentTitle{
                UIView.animate(withDuration: 0.3, animations: {
                    option.isHidden = !option.isHidden
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
    
    @IBAction func donePressed(_ sender: Any) {
        self.currentComment.fraction = Double(self.fraction)
        self.currentComment.text = self.textView.text
        self.delegate?.didFinishEditComment(comment: self.currentComment)
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
