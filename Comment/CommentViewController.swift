//
//  CommentViewController.swift
//  BarMap
//
//  Created by Mojo on 2019/7/10.
//  Copyright © 2019 Mojo. All rights reserved.
//

import UIKit
import MapKit
import GoogleMobileAds

protocol CommentViewControllerDelegate {
    func didFinishComment(store: Store,isEdited: Bool)
}

class CommentViewController: UIViewController {

    let serverConnect = ServerConnect.shared
    let alert = AlertMessage.shared
    let queue = OperationQueue()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var bannerView : GADBannerView!
    
    var comments : [Comment] = []
    var delegate : CommentViewControllerDelegate?
//    var currentAnnotation : MKAnnotation!
//    var storeID : String!
    var currentStore : Store!
    var userAccount = UserDefaults.standard.string(forKey: "userAccount")!
    var isEdited = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let item = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(self.back))
        item.image = UIImage(named: "Back")
        self.navigationItem.leftBarButtonItem = item
        
        self.queryComment()
        
        //AD
        self.bannerView = GADBannerView(adSize: kGADAdSizeBanner)
        self.bannerView.translatesAutoresizingMaskIntoConstraints = false
        self.bannerView.adUnitID = "ca-app-pub-4348354487644961/4047987302" //廣告單元ID
        self.bannerView.rootViewController = self
        self.bannerView.load(GADRequest())
        self.bannerView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if self.comments.isEmpty{
            self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "CommentBackgroundImage")!)
//        }
        self.tableView.backgroundView?.alpha = 0.5
    }
    
    @objc func back(sender: UIBarButtonItem) {
        self.delegate?.didFinishComment(store: self.currentStore, isEdited: self.isEdited)
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func addComment(_ sender: Any) {
        if self.comments.first?.userAccount == self.userAccount{
            self.alert.alert(title: "錯誤", message: "已新增過評論請用\n修改方式更新評論", vc: self)
        }else{
            insertComment()
        }
    }
    
    func insertComment(){
        let comment = Comment()
        comment.storeID = self.currentStore.storeID
        comment.fraction = 5
        comment.userAccount = self.userAccount
        comment.text = ""
        
        let data = ["userAccount":self.userAccount]
        guard let encodedData = try? JSONEncoder().encode(data) else{
            assertionFailure("Fail to encode userAccount.")
            return
        }
        
        self.serverConnect.sentJsonToServer(urlString: "\(serverConnect.domain)queryUserName.php", encodedData: encodedData) { (data) in
            do {
                let user = try JSONDecoder().decode(User.self, from: data)
                print("queryUserName OK")
                comment.userName = user.userName
                
//                DispatchQueue.main.async {
//                    self.tableView.reloadData()
//                }
               
                DispatchQueue.main.async {
                    self.comments.insert(comment, at: 0)
                    let indexPath = IndexPath(row: 0, section: 0)
                    self.tableView.insertRows(at: [indexPath], with: .automatic)
                }
                
            }catch{
                print("error while parsing json \(error)")
            }
            
        }
//        self.comments.insert(comment, at: 0)
        guard let encodedCommentData = try? JSONEncoder().encode(comment) else{
            assertionFailure("Fail to encode comment.")
            return
        }
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)insertComment.php", encodedData: encodedCommentData){ (data) in
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                else{
                    assertionFailure("Fail to get jsonData.")
                    return
            }
            print("insertComment \(String(describing: json["status"]))")
           
//            self.queryComment()
            self.queryCommentFractionAndUpdateStore()

        }
    }
    
    func queryCommentFractionAndUpdateStore(){
        let data = ["storeID":self.currentStore.storeID,"userAccount":userAccount]
        guard let encodedData = try? JSONEncoder().encode(data) else{
            assertionFailure("Fail to encode storeID & userAccount.")
            return
        }
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)queryCommentFraction.php", encodedData: encodedData){ (data) in
            guard let array = try? JSONSerialization.jsonObject(with: data, options: []) as? Array<Double>
                else{
                    assertionFailure("Fail to get jsonData.")
                    return
            }
            print("queryCommentFraction OK")
            
            var sum = 0.0
            var count = array.count
            if self.comments.first?.userAccount == self.userAccount{
                guard let firstFraction = self.comments.first?.fraction else{
                    assertionFailure("Fail to get self.comments.first?.fraction.")
                    return
                }
                sum = firstFraction
                count += 1
            }
            for value in array{
                sum += value
            }

            let avgFraction = count != 0 ? Double(round(10*sum/Double(count))/10) : 0
            self.updateStore(avgFraction:avgFraction)
            self.currentStore.avgFraction = avgFraction
            self.isEdited = true
        }
    }
    
    func updateStore(avgFraction:Double){
        let store = Store()
        store.storeID = self.currentStore.storeID
        store.avgFraction = avgFraction
        print("storeID=\(store.storeID),avgFraction=\(avgFraction)")
        guard let encodedData = try? JSONEncoder().encode(store) else{
            assertionFailure("Fail to encode store.")
            return
        }
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)updateStore.php", encodedData: encodedData){ (data) in
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                else{
                    assertionFailure("Fail to get jsonData.")
                    return
            }
            print("updateStore \(String(describing: json["status"]))")
        }
    }
    
    func queryComment(){
        let data = ["userAccount":self.userAccount,"storeID":self.currentStore.storeID]
        guard let encodedData = try? JSONEncoder().encode(data) else{
            assertionFailure("Fail to encode userAccount & storeID.")
            return
        }
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)queryComment.php", encodedData: encodedData){ (data) in
            do {
                self.comments = try JSONDecoder().decode([Comment].self, from: data)
                print("queryComment OK")
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    
                }
                
            }catch{
                print("error while parsing json \(error)")
            }
        }
        
    }
    
    func updateComment(comment:Comment){
        guard let encodedData = try? JSONEncoder().encode(comment) else{
            assertionFailure("Fail to encode comment.")
            return
        }
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)updateComment.php", encodedData: encodedData){ (data) in
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                else{
                    assertionFailure("Fail to get jsonData.")
                    return
            }
            print("updateComment \(String(describing: json["status"]))")
        }
    }
    
    func deleteComment(comment: Comment){
        guard let encodedData = try? JSONEncoder().encode(comment) else{
            assertionFailure("Fail to encode comment.")
            return
        }
        self.serverConnect.sentJsonToServer(urlString:
        "\(serverConnect.domain)deleteComment.php", encodedData: encodedData){ (data) in
            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                else{
                    assertionFailure("Fail to get jsonData.")
                    return
            }
            print("deleteComment \(String(describing: json["status"]))")
            self.queryCommentFractionAndUpdateStore()
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

//        if segue.identifier == "editCommentSegue"{
//            guard let indexPath = self.tableView.indexPathForSelectedRow else{
//                return
//            }
//            let comment = self.comments[indexPath.row]
//            guard let userAccount = comment.userAccount,
//            userAccount == self.userAccount else{
//                return
//            }
//            let editCommentVC = segue.destination as! EditCommentViewController
//            editCommentVC.currentComment = comment
//            editCommentVC.delegate = self
//        }
    }
    

}

extension CommentViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as! CommentTableViewCell
        cell.userNameLabel.text = self.comments[indexPath.row].userName
        cell.commenTextLabel?.text = self.comments[indexPath.row].text

        guard let userAccount = self.comments[indexPath.row].userAccount else{
            assertionFailure("Fail to get comments[]userAccount.")
            return cell
        }
        
        if userAccount != self.userAccount{
            cell.accessoryType = UITableViewCell.AccessoryType.none
        }
        
        if let fraction = self.comments[indexPath.row].fraction{
            if userAccount == "google"{
                cell.fractionLabel.text = "\(String(format: "%.1f",fraction))"
            }else{
                cell.fractionLabel.text = "\(Int(fraction))"
            }
        }else{
            cell.fractionLabel.text = "-"
        }
        cell.userImageView?.image = UIImage(named: "User")
        
        let imageName = "\(userAccount).jpg"
        guard let url = URL(string: "\(serverConnect.domain)uploads/\(imageName)") else{
            assertionFailure("Fail to get url.")
            return cell
        }
        let operation = UserImageOperation(url: url, tableView: tableView, indexPath: indexPath)
        queue.addOperation(operation)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //相同位置的operation cancel掉 因為已離開畫面
        self.queue.operations.filter { (op) -> Bool in
            let userImageOperation = op as! UserImageOperation
            //如果位置一樣，則取消
            if userImageOperation.indexPath.compare(indexPath) == .orderedSame {
                //                imageOperation.cancel()
                return true
            }
            return false
            }.first?.cancel()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let comment = self.comments[indexPath.row]
        guard let userAccount = comment.userAccount,
            userAccount == self.userAccount else{
                return
        }
        let editCommentVC = self.storyboard?.instantiateViewController(withIdentifier: "EditCommentViewController") as! EditCommentViewController
        editCommentVC.currentComment = comment
        editCommentVC.delegate = self
        self.navigationController?.pushViewController(editCommentVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let comment = self.comments.remove(at: indexPath.row)
            deleteComment(comment: comment)
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if self.comments[indexPath.row].userAccount == self.userAccount{
            return true
        }else{
            return false
        }
    }
    
}

extension CommentViewController: UITableViewDelegate{
    
}

extension CommentViewController: EditCommentViewControllerDelegate{
    func didFinishEditComment(comment: Comment) {
        
        self.updateComment(comment: comment)
        self.queryCommentFractionAndUpdateStore()
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    
}

//MARK: UIGestureRecognizerDelegate
extension CommentViewController: GADBannerViewDelegate {
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        if self.bannerView.superview == nil {
            
            //            self.tableView.tableHeaderView = self.bannerView
            self.topConstraint.isActive = false
            self.view.addSubview(bannerView)
            
            //autolayout
            //廣告上緣－safeArea上緣
            self.bannerView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
            //廣告下緣－tableView上緣
            self.bannerView.bottomAnchor.constraint(equalTo: self.tableView.topAnchor, constant: 0).isActive = true
            //廣告左邊－controller'view 左邊
            self.bannerView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
            //廣告右邊－controller'view 右邊
            self.bannerView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
            
        }
    }
}
